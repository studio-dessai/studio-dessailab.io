---
layout: post
title: "Studio d'essai cxxxix – back"
---

<iframe src="https://archive.org/embed/studio-dessai-2018-12-15" width="500" height="30" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

And we're back! The show left Global Digital after 138 episodes in studio 5 under the Story Bridge in Brisbane. Studio d'essai is now back with the 139<sup>th</sup> episode, produced from the comfort of home, in West End – still in Brisbane.

Today, five tracks: the three first ones are pretty uncomfortable, but rest assured, the two last ones are comparatively soothing.

Delighted to be back, please spread the word, and do take care of yourself! :)

{% include /playlists/{{ page.date | date: "%Y-%m-%d" }}.md %}
