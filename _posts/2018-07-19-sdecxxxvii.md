---
layout: post
title: "Studio d'essai cxxxvii"
---

Another short playlist - we really don't mind long tracks. Two tracks from at least three different places, potentially four: do you know of Sevensy's whereabouts by any chance? Please do let us know!

Be safe and have fun.

{% include /playlists/{{ page.date | date: "%Y-%m-%d" }}.md %}
