---
layout: post
title: "Studio d'essai cxxxviii – Early Dutch Electronic Music"
---

_Head straight to 5:45 for the actual show_

For this episode, Stéphane sifted through the 139 tracks that make up the release _[Popular Electronics: Early Dutch Electronic Music from Philips Research Laboratories 1956-1963](https://musicbrainz.org/release/ce581b49-edfa-40db-bb47-6cccfc9893b4)_ and selected 18 tracks that would fit in an hour.

The tracks are by artists Dick Raaijmakers, Henk Badings, Kid Baltan and Tom Dissevelt, and the final recording is the voice of Fredd Judd sending a spoken letter to Tom Dissevelt.

May you feel safe and content! See you next week.

{% include /playlists/{{ page.date | date: "%Y-%m-%d" }}.md %}
