---
layout: post
title: "Studio d'essai cxxxiv – mix: Shit Mix"
---

The tracks in this mix are all amazing, but the way they were stringed together is terrible. I could give a number of very valid excuses to explain why the mix was executed so poorly, but I'll refrain from it and simply give the mix the name it deserves: _Shit Mix_.

All my excuses to the artists featured.

Enjoy – if you can!

{% include /playlists/{{ page.date | date: "%Y-%m-%d" }}.md %}
