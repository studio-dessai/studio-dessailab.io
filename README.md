# Workflow for posting

* Create a playlist in the _includes/playlists/ directory;
* Write the post for the episode in the _posts/ directory;
* `git add <files>` and `git commit -m "<your_message>"`
* `git push`