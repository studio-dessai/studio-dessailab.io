---
layout: default
---

_Studio d'essai_ was created by [Stéphane Guillou](http://stragu.gitlab.io/) and [Lochlan Morrissey](http://loch.land) in 2014 and started airing on Brisbane's [Global digital](http://4eb.org.au/global) radio on the 5th of June of that year.

In August 2018, after 138 episodes at Global Digital, Stéphane decided that it would be less work for him to keep producing the show from the comfort of home, and to distribute it as podcast-only. Little did he know that it would in fact require even _more_ discipline...
